//Requires
var request = require('request');
var moira = require( 'moira' );


//Global Vars
var APIKey  = "d436d9a12de5418524c1e12e305fc6853ca65";
var Email   = "andrew@andrewpe.com";
var FullDomain  = "apt.andrewpe.com";
var CurrentIP = "";

var minutes = .10, the_interval = minutes * 60 * 1000;
setInterval(GetandSetIP,the_interval);

//Process
//getExternalIP(setCloudFlareIP);


//Functions
function GetandSetIP(){
    getExternalIP(setCloudFlareIP);
}
function getExternalIP(callback){
    console.log("Getting External IP");
    moira.getIP( function( err, ip, service ){
        console.log( 'Your external IP address is ' + ip );
        currentIP = ip;
        callback(ip);
    });
}
function setCloudFlareIP(IP){
    console.log("Starting CloudFlare Update");

    //Breakout
    var self = this;

    //Spliting Domain into Parts
    var domainParts = FullDomain.split('.');
    var subdomain = domainParts.shift();
    var rootDomain = domainParts.join('.');

    //Functions
    this.findCloudflareID = function(callback){
        //Setting up Request
        var r = request.post('https://www.cloudflare.com/api_json.html', function (err, httpResponse, body){
            var response = JSON.parse(body);
            var domains = response.response.recs.objs

            domains.forEach(function(domain){
                if(domain.name == FullDomain){      //Found the right domain
                    console.log("Subdomain Found");
                    console.log("The ID is: " + domain.rec_id);
                    callback(domain.rec_id);
                }
            });

        });

        //Setting up POST data
        var form = r.form();
            form.append('a', 'rec_load_all');
            form.append('tkn', APIKey);
            form.append('email', Email);
            form.append('z', rootDomain);
    }
    this.setCloudFlareRecord = function(CloudflareID){
        //Setting up request
        var r = request.post('https://www.cloudflare.com/api_json.html', function (err, httpResponse, body){
            var response = JSON.parse(body);
                console.log("Result: " + response.result + "!");
                console.log("");

        });

        //Setting up POST data
        var form = r.form();
            form.append('a', 'rec_edit');
            form.append('type', 'A');
            form.append('id', CloudflareID);
            form.append('name', subdomain);
            form.append('content', IP);
            form.append('ttl', 1);
            form.append('service_mode', 0);
            form.append('tkn', APIKey);
            form.append('email', Email);
            form.append('z', rootDomain);
    }

    self.findCloudflareID(self.setCloudFlareRecord);
}